# README #

### What is this repository for? ###

The main purpose of this repo is to provide a fully transparent copy of the code used for the analyses in the article "Dynamics of violent and dehumanizing rhetoric in far-right social media" (Wahlström, Törnberg & Ekbrand, 2020) DOI:10.1177/1461444820952795

Since the this code was put to use facebook has changed its API policy on public groups, see https://developers.facebook.com/blog/post/2018/04/04/facebook-api-platform-product-changes/ so for the code to work you now need to be an administrator of the group you want to access.

### Who do I talk to? ###

* Hans Ekbrand <hans.ekbrand@gu.se>